<?php
error_reporting(0);
$testimonios[] = array(
  'autor' =>  'Selene Lemus González',
  'testimonio' => 'Has llegado a ser una pieza importante en mi crecimiento. Mil gracias y que Dios te llene de bendiciones siempre.',
  'seccion' => 'talleres'
  );

  $testimonios[] = array(
    'autor' =>  'H. Jiménez | Consultante de psicoterapia',
    'testimonio' => 'Agradezco a Dios porque permitió que nuestros caminos se unieran justo en el momento que más lo necesité. Me conforta saber que puedo seguir adelante fuerte, segura y con una esperanza conmigo misma amando mi ser. Gracias por acompañarme en este proceso de transformación.',
    'seccion'=>'psicoterapia'
  );

  $testimonios[] = array(
    'autor' =>  'Mtra. Alejandra García | Directora General Colegio Fray Juan de Zumárraga Cuernavaca',
    'testimonio' => 'Ha sido muy favorable tu presencia en nuestra comunidad educativa por el dinamismo que muestras en cada una de tus conferencias tanto con padres de familia, maestros y alumnos. Este trabajo es fruto de la preparación que tienes dentro de tu ámbito, lo cual reflejas en todo lo que realizas.',
    'seccion' => 'escuelas'
  );

  $testimonios[] = array(
    'autor' =>  'Irma Yolanda Sandoval Miranda | Directora y representante del equipo de Nado Sincronizado del Estado de Morelos',
    'testimonio' => 'Después de las sesiones de desempeño óptimo, las niñas se llevaron mejor entre ellas, lograron ser mejores compañeras y trabajar mejor en equipo, lograron tener un propósito y un sentido, fueron más tranquilas y mucho más seguras a la competencia y sobre todo hubo un mayor disfrute.',
    'seccion' => 'deportistas'
  );

  $testimonios[] = array(
    'autor' =>  'Pilar Rosales del Ángel | Auditora de Ventas Placacentro Morelos.',
    'testimonio' => 'Estoy muy agradecida contigo, por  incluir un valor agregado,  por dar
    ese "plus" que caracterizan a ciertas personas y las hacen prácticamente
    inolvidables. Gracias de corazón.',
    'seccion'=>'conferencias'
  );

  $testimonios[] = array(
    'autor' =>  'Ing. G. Lucio Ramírez | Director General Avantec Inmobiliaria',
    'testimonio' => 'Gracias por darme la lija para pulir mi piedra.',
    'seccion'=>'empresas'
  );

  $testimonios[] = array(
    'autor' =>  'Ing. G. Lucio Ramírez | Director General Avantec Inmobiliaria',
    'testimonio' => 'En la vida del ser humano, recibimos bendiciones, y se dice que tenemos ángeles a nuestro alrededor, gracias por ser uno de ellos.',
    'seccion'=>'empresas'
  );

  $testimonios[] = array(
    'autor' => 'Laura Olivia Quiroz Aguilar | Docente Colegio Fray Juan de Zumárraga Cuernavaca',
    'testimonio' => 'Durante las sesiones realmente me sentí privilegiada, cada una fue distinta con dinámicas muy significativas las cuales me hacían aplicar más mi conciencia en situaciones complicadas con mis alumnos.  '
  );




function isActive($ruta){

  $seccion = basename($_SERVER["REQUEST_URI"], ".php");
  if ($seccion == $ruta) {
    echo 'class="active"';
  }
}


require 'mailer.php';

if (isset($_POST['email'])) {

  $email  = $_POST['email'];
  $nombre  = $_POST['name'];
  $mensaje = $_POST['message'].'<br>Contactarse con: '.$email;
  //Create a new PHPMailer instance
  $mail = new PHPMailer();
  //Set who the message is to be sent from
  $mail->charSet = "UTF-8";
  $mail->SetFrom($email,'Formulario Web: '.$nombre);
  //Set an alternative reply-to address
  // $mail->AddReplyTo($correo,$nombre);
  //Set who the message is to be sent to
  $mail->AddAddress('selmen.nicolle@gmail.com','Formulario Web Nicolle Selmen: '.$nombre);
  //Set the subject line
  $mail->Subject ='Formulario Web: '.$nombre;
  $mail->Body = $mensaje;
  $mail->AltBody = $mensaje;
  if(!$mail->Send()) {
    $err= "Ocurrió un error, intenta de nuevo más tarde";
  } else {
    $msj= "Mensaje enviado correctamente";
  }
}



$testimonio = $testimonios[array_rand($testimonios)];

$ruta_actual = basename($_SERVER["REQUEST_URI"], ".php");
if ($ruta_actual==''||$ruta_actual=='nicolle') {
  $ruta_actual='index';
}

foreach ($testimonios as $key => $value) {
  if ($value['seccion']==$ruta_actual) {
    $testimonio['autor'] = $value['autor'];
    $testimonio['testimonio'] = $value['testimonio'];
  }
}

if (date('Y-m-d')>'2015-06-09') {
  header('location:inicio.html');
}
?>
