<?php require 'header.php';?>
<div class="topBanner">
  <img src="img/banners/conferencias.jpg" alt="Banner conferencias" width="100%" />
</div>


  <div class="air"></div>


  <div class="container">
    <div class="contenido">



      <div class="row">
        <div class="col-md-7">
          <h2 class="texto_morado">
            <img class="icono" src="img/iconos/conferencias.png" alt="Icono conferencias" />
            Conferencias Vivenciales</h2>
          <p class="texto_morado">
            Buscan impactar a un gran número de personas y que éste impacto perdure en el tiempo.
          </p>
          <p>
            A través de las conferencias vivenciales se siembra una semilla, a través del aprendizaje significativo, utilizando diversas dinámicas, en las que el público <strong>vive la experiencia del mensaje transmitido</strong>.
          </p>
          <p>
            Cada conferencia se construye a la medida, de acuerdo a las necesidades y a los objetivos que se desean alcanzar.
          </p>
        </div>


        <div class="col-md-5 lateral">
          <img src="img/conferencias/1.jpg" alt="Conferencias" width="100%"/>
          <div class="space"></div>
          <img src="img/conferencias/2.jpg" alt="Conferencias" width="100%"/>
        </div>

      </div>

    </div>
  </div><!-- /.container -->




  <?php require 'testimonios.php';?>
  <?php require 'servicios.php';?>





<?php require 'footer.php';?>
