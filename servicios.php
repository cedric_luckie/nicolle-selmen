<div class="contenido">
  <div class="row" class="servicios">
    <div class="col-md-2 col-xs-6 servicio">
      <a href="./psicoterapia" class="texto_naranja">
        <img class="icono" src="img/iconos/psicoterapia.png" alt="Icono talleres" />
        Psicoterapia & Coaching
      </a>
    </div>
    <div class="col-md-2 col-xs-6 servicio">
      <a href="./talleres" class="texto_naranja">
        <img class="icono" src="./img/iconos/talleres.png" alt="Talleres al público" />
        Talleres al Público
      </a>
    </div>
    <div class="col-md-2 col-xs-6 servicio">
      <a href="empresas" class="texto_naranja">
        <img class="icono" src="./img/iconos/empresas.png" alt="Empresas" />
        Empresas
      </a>

    </div>
    <div class="col-md-2 col-xs-6 servicio">
      <a href="escuelas" class="texto_naranja">
        <img class="icono" src="./img/iconos/escuelas.png" alt="Escuelas" />
        Escuelas
      </a>

    </div>
    <div class="col-md-2 col-xs-6 servicio">
      <a href="deportistas" class="texto_naranja">
        <img class="icono" src="./img/iconos/deportistas.png" alt="Deportistas" />
        Deportistas
      </a>
    </div>
    <div class="col-md-2 col-xs-6 servicio">
      <a href="conferencias" class="texto_naranja">
        <img class="icono" src="./img/iconos/conferencias.png" alt="Conferencias vivenciales" />
        Conferencias Vivenciales
      </a>

    </div>
  </div>
</div>
