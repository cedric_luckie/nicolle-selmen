<div class="air"></div>
<div class="fulldiv <?=$ruta_actual?>">
  <?php if ($ruta_actual=='index'): ?>
    <div class='row'>
      <div class='col-md-offset-1 col-md-11'>
        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
          <!-- Bottom Carousel Indicators -->
          <ol class="carousel-indicators hidden">
            <?php
            for ($i=0; $i < count($testimonios); $i++) {
              $active = $i==0?'class="active"':'';
              echo '<li data-target="#quote-carousel" data-slide-to="'.$i.'" '.$active.'></li>';
            } ?>
          </ol>
          <!-- Carousel Slides / Quotes -->
          <div class="carousel-inner testimonios">
            <!-- Quote 1 -->
            <?php
            $j=0;
            foreach ($testimonios as $key => $value):
              $active = $j==0?'active':'';
              ?>
              <div class="item <?=$active?>">
                <blockquote>
                  <p>
                    <i class="fa fa-quote-left"></i>
                    <?=$value['testimonio'] ?>
                    <i class="fa fa-quote-right"></i>
                  </p>
                  <small style="color:#fff"><?php echo $value['autor'] ?></small>
                </blockquote>
              </div>
              <?php
              $j++;
              endforeach ?>

            </div>

            <!-- Carousel Buttons Next/Prev -->
            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
          </div>
        </div>
      </div>
    <?php endif ?>
    <?php if ($ruta_actual!='index'): ?>
      <p class="lead">
        <i class="fa fa-quote-left"></i> <?=$testimonio['testimonio']; ?>
        <i class="fa fa-quote-right"></i>
      </p>
      <p class="pull-right">
        <?=$testimonio['autor']; ?>
      </p>
    <?php endif ?>
  </div>

  <div class="air"></div>
