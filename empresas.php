<?php require 'header.php';?>
<div class="topBanner">
  <img src="img/banners/empresas.jpg" alt="Banner empresas" width="100%" class="topBanner" />
</div>


  <div class="air"></div>





  <div class="container">
    <div class="contenido">



      <div class="row">
        <div class="col-md-7">
          <h2 class="texto_morado">
            <img class="icono" src="img/iconos/empresas.png" alt="Icono empresas" />
            Empresas</h2>

            <p>
              Hoy en día, las empresas se desenvuelven dentro de un <font>entorno globalizado</font>, altamente <font>competitivo</font> y, por demás <font>demandante</font>, debido a la cantidad de servicios y productos que se ofrecen en el mercado. Para poder permanecer, crecer y estar por encima de sus competidores, éstas se ven en la necesidad de <font>reinventarse</font>, asumiendo constantes retos en busca de una mejora <font>continua y significativa</font>. Es por ello que para lograrlo apuestan por potenciar lo que les ofrece las mayores ventajas: el <font>Capital Humano</font>.
            </p>

          <p>
            ASPECTOS A EVALUAR Y POTENCIAR EN EL CAPITAL HUMANO DE UNA EMPPRESA
          </p>
          <ul>
            <li>Motivación, productividad y rendimiento del personal</li>
            <li>Compromiso de los colaboradores</li>
            <li>Satisfacción de los colaboradores</li>
            <li>Ambiente laboral</li>
            <li>Cumplimiento de las metas personales y de la empresa</li>
            <li>Nivel de eficiencia de los procesos</li>
            <li>Estructura organizacional</li>
            <li>Relaciones interpersonales</li>
            <li>Capacidad de liderazgo</li>

          </ul>
        </div>


        <div class="col-md-5 lateral">
          <div class="panel panel-default">
            <div class="panel-heading empresas">
              <h3 class="panel-title">SERVICIOS PARA EMPRESAS</h3>
            </div>
            <div class="list-group">
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#impacto">Talleres a la medida de Alto Impacto</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#desarrollo">Desarrollo organizacional</a>

            </div>

          </div>
          <img src="img/empresas/1.jpg" alt="Deportistas" width="100%"/>
          <div class="space"></div>
          <img src="img/empresas/2.jpg" alt="Deportistas" width="100%"/>
        </div>

      </div>

    </div>
  </div><!-- /.container -->



  <?php require 'testimonios.php';?>
  <?php require 'servicios.php';?>



  <div class="modal fade" id="impacto" tabindex="-1" role="dialog" aria-labelledby="sintoma" aria-hidden="true">
    <div class="modal-dialog empresas">
      <div class="modal-content empresas">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title  " id="myModalLabel">Talleres a la medida de Alto Impacto</h4>
        </div>
        <div class="modal-body">
          <p>
            Habiendo identificado las <font>necesidades específicas</font> y los objetivos de la empresa, se crea un taller de capacitación que satisfaga las expectativas solicitadas, con la finalidad de potenciar las aptitudes laborales y las actitudes positivas del personal, logrando en él una <font>transformación que perdure en el tiempo</font> y se traduzca en satisfacción, productividad y mayor compromiso con la empresa.
          </p>
          <p>
            TALLERES RECOMENDADOS Y MÁS SOLICITADOS POR LAS EMPRESAS:
          </p>
          <ul>
            <li>Desarrollo del Potencial Humano</li>
            <li>Formación de Líderes</li>
            <li>Manejo del Estrés</li>
            <li>Habilidades Gerenciales</li>
            <li>Creatividad</li>
            <li>Comunicación que Genera Posibilidades</li>
            <li>El Arte de Servir al Cliente</li>
            <li>Manejo y Resolución de Conflictos</li>
            <li>Colaboración y Trabajo en Equipo</li>
            <li>Desempeño Óptimo y Optimización de Equipos de Trabajo</li>
            <li>Desarrollo de  las Inteligencias Múltiples</li>
            <li>Imagen Personal Corporativa: Una empresa es lo que sus colaboradores representan</li>
            <li>Inteligencia Emocional</li>
            <li>¿Cómo me relaciono con la vida y qué hay detrás?</li>
            <li>Salud Física y Emocional: Balance Vital</li>
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="desarrollo" tabindex="-1" role="dialog" aria-labelledby="sintoma" aria-hidden="true">
    <div class="modal-dialog empresas">
      <div class="modal-content empresas">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title  " id="myModalLabel">Desarrollo organizacional</h4>
        </div>
        <div class="modal-body">
          <p>
            Este programa es el más recomendado cuando se quiere <font>potenciar a la institución o empresa</font>, abarcando todas las áreas y a todo el personal. Para ello se utiliza  un <font>diagnóstico cuantitativo y cualitativo</font>, con el que se  identifican las <font>áreas de oportunidad</font>  que existen.
          </p>
          <p>
            Se cuenta con un <font>Sistema de Semaforización</font> que indica la situación real de la institución, usando el siguiente señalamiento:
          </p>
          <ul>
            <li >Verde (Estado Sano)</li>
            <li >Amarillo (Estado de Alerta)</li>
            <li >Rojo ( Estado de Peligro)</li>
          </ul>
          <p>
            El Sistema de Semaforización arroja resultados cuantitativos, utilizando un cuestionario que evalúa catorce factores fundamentales de la empresa, a nivel Estructural y Social. En una entrevista individual, a los colaboradores de la empresa se les aplica un Cuestionario de Detección de Necesidades de Capacitación que finalmente arrojará los resultados cualitativos.
          </p>
          <p>
            El Desarrollo Organizacional utiliza prácticas innovadoras y de vanguardia,  en un proceso que se da en cinco fases, teniendo una duración mínima de tres meses:
          </p>
          <p>
            Fase 1: Diagnóstico y Detección de Necesidades de Capacitación <br>
            Fase 2: Entrega de Resultados y Plan de Trabajo<br>
            Fase 3: Implementación del Plan de Trabajo<br>
            Fase 4: Evaluación y Resultados<br>
            Fase 5: Seguimiento<br>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <style media="screen">
  .modal-body{
    color:#fff;
    padding:20px;
  }
  </style>

<?php require 'footer.php';?>
