<?php require 'header.php';?>
<div class="topBanner">
  <img src="img/banners/talleres.jpg" alt="Banner talleres" width="100%"/>
</div>

  <div class="air"></div>


    <div class="container">
      <div class="contenido">



        <div class="row">
          <div class="col-md-8">
            <h2 class="texto_morado">
              <img class="icono" src="img/iconos/talleres.png" alt="Icono talleres" />
              Talleres al público</h2>

              <h3 class="texto_morado">Los talleres vivenciales son generadores de cambio.            </h3>
            <p>
              La experiencia nos dice que generalmente las personas hacemos un cambio en nuestra persona cuando ya antes lo hizo otra. Permanecemos estáticos con la esperanza de que el cambio personal sea una simple respuesta y no un impulso auténtico e innato.
            </p>
            <p>
              Justamente, los talleres ayudan a que se generen esos cambios por convicción, pues la verdadera transformación del ser radica en su interior. Los talleres vivenciales promueven un crecimiento personal, a través del autoconocimiento, del contacto consigo mismo y del responsabilizarse de sí, lo que mejora la calidad de vida y las relaciones intrapersonales e interpersonales.
            </p>
            <p>
              Si se es capaz de observarse, reconocerse y hacerse cargo de sí y de sus decisiones, se es capaz de generar magníficos cambios en la vida, con el apoyo de los siguientes talleres:
            </p>

            <h4 onclick="$('.bellezaMujer').slideToggle()">La belleza de ser mujer</h4>
            <div class="hid bellezaMujer">
              <p>
                La sensibilidad de la mujer es una de sus enormes cualidades. Ésta le permite desarrollar un espíritu de autocrítica que puede llevarle a conducirse con seguridad o con inseguridad en las distintas etapas y ámbitos de su vida. Si en algún momento esta autocrítica provoca baja autoestima, es momento de recurrir al apoyo de un profesional que le ayude a redescubrirse.
              </p>
              <p>
                Este taller ofrece  herramientas y técnicas innovadoras para que se recupere o reafirme la autoestima, cambiando el sistema de autopercepción.
              </p>
              <p>
                Le invito a que analice las siguientes preguntas:
              </p>
              <p>
                ¿Alguna vez se has visto en el espejo y no le gusta lo que ve?, ¿se ha sentido poco valiosa?, ¿se compara con otras mujeres y no se siente suficiente?, ¿le gustaría sentirse más plena consigo misma?, ¿le gustaría ser capaz de redescubrirse y ver su potencialidad?, ¿le gustaría sentir una profunda auto-aceptación?
              </p>
              <p>
                Si contestó que sí a más de tres  preguntas, este taller es para usted.
              </p>
            </div>


            <h4 onclick="$('.vidaNueva').slideToggle()">Año Nuevo, Vida Nueva</h4>
            <div class="hid vidaNueva">
              <p>
                No es raro que escuchemos a las personas haciendo planes para un futuro que se queda solamente en eso: planes. Desgraciadamente, tomar decisiones asertivas y prometedoras genera inseguridad, pereza o miedo porque no se cuenta con las herramientas y técnicas necesarias que faciliten su implementación y aseguren su éxito. Es por esto que, el contar con un equipo de apoyo y personal calificado hace que sean alcanzables  los propósitos y se lleven a cabo de acuerdo a sus expectativas. Este taller, justamente ofrece un plan que le involucra, le estimula y optimiza su logro.
              </p>
              <p>
                Le invito a que analice las siguientes preguntas:
              </p>
              <p>
                ¿Ha sentido que carga con cosas del pasado y le es <b>difícil soltar</b>?, ¿siente que hay   asuntos que <b>faltan por cerrar</b>?, ¿le gustaría sentirse   <b>ligero/a y libre</b>, con recursos que potencialicen su vida futura y le ayuden a conseguir un mayor bienestar?
              </p>
              <p>
                Sí contestó que sí a más de tres  preguntas, este taller es para usted.
              </p>
            </div>


            <!-- <h4 onclick="$('.cambio').slideToggle()">El cambio inicia con uno mismo</h4>
            <div class="hid cambio">
              <p>
                Ofrecemos talleres vivenciales que  promuevan un crecimiento personal, autoconocimiento y contacto consigo mismo para mejorar tu calidad de vida y tu relación intrapersonal e interpersonal. Algunos de ellos son:
              </p>
            </div> -->


            <h4 onclick="$('.detras').slideToggle()">¿Cómo me relaciono con la vida y que hay detrás?</h4>
            <div class="hid detras">
              <p>
                Existen pautas que se utilizan como instrumento o guía para relacionarse con el otro y con la vida. Esas pautas pueden estar en un nivel funcional o disfuncional, provocando circunstancias o situaciones a su favor o en su contra. Modificar conductas, incrementar la asertividad en la toma de decisiones y generar los cambios esperados para mantener relaciones armónicas no es tan fácil; muchas veces requiere del apoyo de un orientador; por lo mismo, le invito a que analice las siguientes preguntas:
              </p>
              <p>
                ¿Le gustaría explorar desde <b>dónde se relaciona</b> con el otro y qué es lo que está originando desde ahí?,  ¿quisiera  aprender a vincularse de manera consciente, siendo quien es realmente y no desde sus mecanismos de defensa, <b>generando relaciones auténticas y sanas</b>? Si es así este taller es para usted.
              </p>

            </div>


            <h4 onclick="$('.miEleccion').slideToggle()">Mi elección, mi pasión, mi futuro</h4>
            <div class="hid miEleccion">
              <p>
                <i>“El futuro se construye con el hoy”</i>. Esta sabia frase encierra la idea más clara de lo que se puede esperar del futuro en los ámbitos personal, social y laboral, al confrontarle con la asertividad que se requiere para tomar una decisión  trascendental, como es la elección de una profesión que esté de acuerdo a tus habilidades, inteligencias y expectativas.
              </p>
              <p>
                Si estás en busca de una claridad acerca de la elección de qué carrera estudiar, <b>encontrando</b> lo que verdaderamente <b>te apasiona</b> para construir tu futuro y que esté acorde a tu <b>esencia personal</b>, este taller es para ti.
              </p>
            </div>


            <h4 onclick="$('.espejo').slideToggle()">El espejo de tu vida es tu cuerpo</h4>
            <div class="hid espejo">
              <p>
                ¿Sabías que tu salud física está directamente relacionada con tu salud emocional? ¿Has probado mil dietas y nada te funciona? ¿Sabes llevar una buena alimentación? ¿Te gustaría alimentarte sano, delicioso y al mismo tiempo bajar de peso? ¿Te gustaría descubrir si existe algo a nivel emocional y psicológico que te impide bajar de peso? Si es así este taller es para ti.
              </p>
              <p>
                Taller facilitado en conjunto con un médico especialista en obesidad y sobrepeso.
              </p>
            </div>
          </div>


          <div class="col-md-4 lateral">
            <img src="img/talleres/2.jpg" alt="Talleres al público" width="100%"/>
            <div class="space"></div>
            <img src="img/talleres/1.jpg" alt="Talleres al público" width="100%"/>
            <div class="space"></div>
            <img src="img/talleres/3.jpg" alt="Talleres al público" width="100%"/>
            <div class="space"></div>
            <img src="img/talleres/4.jpg" alt="Talleres al público" width="100%"/>
            <div class="space"></div>
            <img src="img/talleres/5.jpg" alt="Talleres al público" width="100%"/>
          </div>

        </div>

      </div>
    </div><!-- /.container -->

  <?php require 'testimonios.php';?>

  <?php require 'servicios.php';?>


<style media="screen">
h4{
  padding:7px;
}
h4:hover{
  color:#fff;
  cursor:pointer;
  background-color:#8d2166;
  width:100%;
}
.hid{
  display:none;
}
</style>


<?php require 'footer.php';?>
