<?php require 'header.php';?>
<div class="topBanner">
  <img src="img/banners/deportistas.jpg" alt="Banner deportistas" width="100%" class="topBanner" />
</div>
  <div class="air"></div>


  <div class="container">
    <div class="contenido">



      <div class="row">
        <div class="col-md-7">
          <h2 class="texto_morado">
            <img class="icono" src="img/iconos/deportistas.png" alt="Icono deportistas" />
            Deportistas</h2>
          <h3>PROGRAMA DE DESEMPEÑO ÓPTIMO </h3>
          <p>
            El objetivo de este programa es lograr que los deportistas se mantengan <b>centrados y enfocados en su objetivo</b>; esto les conduce a sentirse <b>satisfechos y plenos</b>, emocionalmente, para poder alcanzar un nivel de desempeño óptimo, individual y grupal; ya que éste programa <b>explora las creencias limitantes</b>, los <b>miedos</b> y confronta los <b>logros y las metas</b> con el fin de que los deportistas se desenvuelvan, tanto en entrenamientos como en competencias con <b>todo su potencial</b>.
          </p>

          <p>
            Dicho  programa de Desempeño Óptimo tiene tres líneas estratégicas:
          </p>

          <h4>1. Trabajo con los deportistas y el entrenador de manera grupal</h4>
          <p>
            La <b>integración</b> es fundamental para un equipo; <b>ya que es cuando las partes constituyen un todo</b>, pero sin perder la individualidad de cada uno de los integrantes.
          </p>
          <p>
            El logro de los objetivos de un equipo deportivo radica justamente en esa integración; pues sin ella, generalmente se resta el potencial del equipo, en lugar de sumarlo.
          </p>
          <p>
            Aun cuando se compita de forma individual, la compenetración y  solidaridad de todo el equipo es indispensable para el deportista, al ser su equipo una red de apoyo.
          </p>

          <h4>2. Trabajo con los deportistas y el entrenador en sesiones individuales</h4>
          <p>
            Para lograr una verdadera potenciación en el desempeño es importante sumar el trabajo individual, al trabajo en grupo. No hay que olvidar que cada ser humano es un universo; por ende, pueden tener creencias limitantes y eventos traumáticos, inconscientes o conscientes, que asociados a estas creencias generan reacciones emocionales, tales como bloqueos, quedarse paralizado, sentir desconfianza,  inseguridad, miedo, etc.
          </p>
          <p>
            A través de las modalidades terapéuticas utilizadas en el trabajo con los deportistas y entrenadores, se logra reprocesar y desensibilizar los eventos traumáticos y desarticular las creencias negativas para después instalar recursos positivos que potencialicen el desempeño.
          </p>

          <h4>3. Trabajo con los padres</h4>
          <p>
            La importancia del apoyo de los padres para la potenciación del desempeño deportivo y autoestima de sus hijos es fundamental. Los padres de alguna manera hacen que la elección deportiva de su hijo se lleve a la práctica. Sin embargo, en muchas ocasiones ciertas creencias, sobre-exigencias, desatención o falta de apoyo pueden limitar el éxito del niño o joven, en todos los aspectos.
          </p>


          <p class="texto_amarillo">
            Cada ser humano es único y presenta necesidades distintas, por lo que los proyectos de desempeño óptimo se hacen a la medida, ya sea grupal o individual.
          </p>

        </div>


        <div class="col-md-5 lateral">
          <div class="air">

          </div>
          <img src="img/deportistas/0.jpg" alt="Deportistas" width="100%"/>
          <div class="space"></div>
          <img src="img/deportistas/1.jpg" alt="Deportistas" width="100%"/>
          <div class="space"></div>
          <img src="img/deportistas/2.jpg" alt="Deportistas" width="100%"/>
          <div class="space"></div>
          <img src="img/deportistas/3.jpg" alt="Deportistas" width="100%"/>
          <div class="space"></div>

        </div>

      </div>

    </div>
  </div><!-- /.container -->



  <?php require 'testimonios.php';?>
  <?php require 'servicios.php';?>





<?php require 'footer.php';?>
