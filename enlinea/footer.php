<div class="air"></div>

  <?php if ($ruta_actual=='index'||$ruta_actual==''){ ?>
    <div class="footer">
  <?php }else{ ?>
    <div class="footer2">
  <?php } ?>

  <div class="row" style="max-width:80%">
    <div class="col-md-8">
      <div class="contactInfo">
        <p class="lead">
          Nicolle Selmen Chattaj
        </p>
        <p>
          Puebla, Puebla, México
        </p>
        <p>
          Celular: 044 222 630 1923
        </p>
        <p>
          Correo: info@nicolleselmen.com
        </p>
        <p class="hidden">
          <!-- <a class="btn btn-default btn-trans" href="#"><i class="fa texto_negro fa-facebook"></i></a>
          <a class="btn btn-default btn-trans" href="#"><i class="fa texto_negro fa-twitter"></i></a> -->
          <a class="btn btn-default btn-trans" href="http://mx.linkedin.com/pub/nicolle-selmen-chattaj/95/451/273" target="_blank"><i class="fa texto_negro fa-linkedin"></i></a>
          <a class="btn btn-default btn-trans" href="https://www.youtube.com/channel/UCBLHG3yzxea5bKf3GiW6irw" target="_blank"><i class="fa texto_negro fa-youtube"></i></a>
        </p>
      </div>
    </div>
    <div class="col-md-4">
      <?php if ($ruta_actual=='index'){ ?>
        <form action="" method="post">
          <input type="text" name="name" class="form-control inputFoot" placeholder="Nombre">
          <input type="text" name="email" class="form-control inputFoot" placeholder="Tel / Email">
          <textarea name="message" class="message inputFoot" placeholder="Comentario"></textarea>
          <input type="submit" name="submit_form" value="Enviar" class="btn btn-default inputFoot" style="width:100%;color:#fff">
        </form>
      <?php }else{ ?>
        <form action="" method="post">
          <input type="text" name="name" class="form-control inputFootGris" placeholder="Nombre">
          <input type="text" name="email" class="form-control inputFootGris" placeholder="Tel / Email">
          <textarea name="message" class="message inputFootGris" placeholder="Comentario"></textarea>
          <input type="submit" name="submit_form" value="Enviar" class="btn inputFoot" style="width:100%;color:#fff">
        </form>
      <?php } ?>

    </div>
  </div>
</div>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="./js/bs.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="./js/ie10-workaraound.js"></script>
<script src="./js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/jquery.flexslider-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/0.3.3/sweet-alert.min.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=299606170246717&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
<?php if ($msj): ?>
  swal('Listo','Tu mensaje ha sido enviado, en breve nos pondremos en contacto contigo','success');
<?php endif ?>
<?php if ($err): ?>
  swal('Error','Ocurrió un error al enviar el mensaje, revisa que tu correo esté escrito correctamente','error');
<?php endif ?>
</script>


<!-- Place in the <head>, after the three links -->
</body>
</html>
