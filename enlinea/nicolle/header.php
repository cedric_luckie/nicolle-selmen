<?php require 'variables.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="title" content="Nicolle Selmen">
  <link rel="shortcut icon" href="./img/favi.png" type="image/x-icon">
  <link rel="icon" href="./img/favi.png" type="image/x-icon">
  <meta name="description" content="Nicolle Selmen es promotora del desarrollo del potencial humano a nivel individual y organizacional, por medio del coaching y la psicoterapia especializada, el desarrollo de talleres, grupos de sensibilización, conferencias y programas de desarrollo organizacional.">
  <meta name="author" content="Nicolle Selmen">

  <title>Nicolle Selmen</title>

  <!-- Bootstrap core CSS -->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/flexslider/2.2.2/flexslider.css" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/0.3.3/sweet-alert.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body>

    <?php if ($ruta_actual=='index'){ ?>
      <div class="header">
        <a href="index">
          <img src="img/nicolle.png" alt="Nicolle Selmen Logo" align="center"  />
        </a>
      </div>
    <?php }else{ ?>
      <div class="header2">
        <div class="row">
          <div class="col-md-1 hidden-xs"></div>
          <div class="col-md-3 col-xs-6">
            <a href="index" class="logo_interior">
              <img src="img/nicolle2.png" style="text-align:center !important" alt="Nicolle Selmen Logo" />
            </a>
          </div>
          <div class="col-md-4 hidden-xs">
            <h4 align="center" class="eslogan texto_morado">pule tu piedra</h4>
          </div>
          <div class="col-md-3 col-xs-5">
            <div class="eslogan pull-right">
              <a href="https://www.facebook.com/nicolleselmen" target="_blank"><i class="fa fa-facebook social_icon"></i></a>
              <a href="http://mx.linkedin.com/pub/nicolle-selmen-chattaj/95/451/273" target="_blank"><i class="fa fa-linkedin social_icon"></i></a>
              <a href="https://www.youtube.com/channel/UCBLHG3yzxea5bKf3GiW6irw" target="_blank"><i class="fa fa-youtube social_icon"></i></a>
            </div>
          </div>
        </div>

      </div>
    <?php } ?>
  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li <?php isActive('psicoterapia') ?>><a href="psicoterapia">Psicoterapia & Coaching</a></li>
          <li <?php isActive('talleres') ?>><a href="talleres">Talleres al Público</a></li>
          <li <?php isActive('empresas') ?>><a href="empresas">Empresas</a></li>
          <li <?php isActive('escuelas') ?>><a href="escuelas">Escuelas</a></li>
          <li <?php isActive('deportistas') ?>><a href="deportistas">Deportistas</a></li>
          <li <?php isActive('conferencias') ?>><a href="conferencias">Conferencias</a></li>
          <li <?php isActive('social') ?>><a href="social">Social Media</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
