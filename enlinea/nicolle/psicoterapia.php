<?php require 'header.php';?>
<div class="topBanner">
  <img src="img/banners/psico2.jpg" alt="Psicoterapia & coaching" width="100%" />
</div>

  <div class="air"></div>


  <div class="container">
    <div class="contenido">



      <div class="row">
        <div class="col-md-7">
          <h2 class="texto_morado">
            <img class="icono" src="img/iconos/psicoterapia.png" alt="Icono psicoterapia" />
            Psicoterapia & Coaching</h2>
          <p>
            Estos dos métodos que se utilizan para apuntalar el descubrimiento y el desarrollo del potencial humano permiten que la persona se explore, se redescubra y por sí misma encuentre sus propios recursos de autoapoyo.
          </p>

          <h4>PSICOTERAPIA HUMANISTA</h4>
          <p>
            El Proceso Terapéutico Humanista o Terapia Gestalt es como un encuentro entre “el tú y el yo”. Esta terapia acompaña para que se logre la <strong>autoexploración, el redescubrimiento y el encuentro</strong> de los propios recursos que facilitan el desarrollo del potencial humano, <strong>liberándose</strong> de las circunstancias que disminuyen o limitan los niveles de satisfacción, autorrealización y buen funcionamiento; así como también,  de las “máscaras” que se utilizan como recurso para adaptarse y que  hacen perder de vista la <strong>esencia personal</strong>.
          </p>

          <h4>COACHING ONTOLÓGICO MULTIDIMENSIONAL</h4>
          <p>
            Es un método de aprendizaje que <strong>incrementa  la conciencia</strong> sobre el impacto que tienen las acciones en la vida de la persona y en los distintos sistemas en que se interactúa.
          </p>
          <ul>
            <li><strong>Desarrolla</strong> las competencias del coachee para que genere acciones diferentes, en beneficio del rendimiento individual y grupal.</li>
            <li><strong>Potencializa</strong> la estructura mental y la matriz interpretativa del coachee.</li>
            <li><strong>Incrementa</strong> su capacidad reflexiva.</li>
            <li><strong>Enfatiza</strong> la brecha existente entre la intención y el resultado.</li>
          </ul>

        </div>


        <div class="col-md-5 lateral">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Técnicas utilizadas</h3>
            </div>
            <div class="list-group">
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#persona">Enfoque centrado en la Persona</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#sintoma">Trabajo Gestático con el Síntoma</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#sueños">Trabajo Gestático con Sueños</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#niños">Trabajo Gestático con Niños Y Adolescentes</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#gestalt">Sensibilización Gestalt</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#constelaciones">Constelaciones Familiares</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#sincronizacion">Sincronización Cerebral</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#energetica">Psicoterapia Energética</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#transpersonal">Psicoterapia Transpersonal</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#tanatologia">Tanatología</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#programacion">Programación Neurolingüística</a>

            </div>
          </div>
          <!-- <img src="img/psico/1.jpg" alt="Psicoterapia & coaching" width="100%"/><hr> -->
          <img src="img/psico/2.jpg" alt="Psicoterapia & coaching" width="100%"/>
        </div>

      </div>

    </div>
  </div><!-- /.container -->

  <?php require 'testimonios.php';?>
  <?php require 'servicios.php';?>




<!-- Modales -->

<div class="modal fade" id="persona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Enfoque centrado en la persona</h4>
      </div>
      <div class="modal-body">
        <p>
          Esta técnica facilita el proceso de orientación. Fue creada por Carl Rogers y emplea <strong>actitudes</strong> que favorecen la <strong>toma de conciencia</strong>, la responsabilidad y el <strong>contacto</strong>.  Aunada a ella,  se aplican otras herramientas de intervención que facilitan la comunicación humana y la comprensión profunda en las relaciones que se establecen.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="sincronizacion" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Sincronización Cerebral</h4>
      </div>
      <div class="modal-body">
        <p>
          La psicoterapia contemporánea ha generado técnicas que ayudan a procesar, con enorme eficacia y rapidez, los asuntos que impiden nuestro funcionamiento óptimo. Una de estas técnicas es la Sincronización Cerebral (BST), cuya finalidad es <strong>disminuir el estado de perturbación del consultante</strong>. Esta técnica permite procesar los recuerdos traumáticos; reorganizar y comprender las experiencias externas e internas de manera funcional, obteniendo alivio, bienestar e integración de su ser. Se reducen <strong>la ansiedad, los temores, el enojo, los pensamientos intrusivos, los traumas, las creencias limitantes y el estrés postraumático</strong>.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="sueños" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Trabajo Gestáltico con Sueños</h4>
      </div>
      <div class="modal-body">
        <p>
          Para Fritz Perls el <strong>sueño es un mensaje existencial</strong>, cuyo significado se hace evidente, si el soñante está dispuesto a trabajarlo.  Perls considera que éste es un <strong>proceso de sanación</strong>, pues <strong>expresa algo que está presente</strong> en la persona. Para el trabajo gestáltico con sueños lo <strong>externo es interno</strong>, por lo que se trabaja para recuperar esta proyección, permitiendo integrarse y saber más de sí mismo.
        </p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="energetica" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Psicoterapia Energética</h4>
      </div>
      <div class="modal-body">
        <p>
          Esta técnica <strong>ayuda a reequilibrar el campo energético</strong>. Es la suma de la Psicoterapia convencional de Occidente y la Psicoterapia Oriental, que se centra en el cuerpo y la mente.
        </p>
        <p>
          Desde el punto de vista de la Psicoterapia Energética los <strong>síntomas emocionales, físicos y psico-espirituales</strong> <strong>son el resultado de una perturbación o interrupción del campo energético</strong>, por lo que se tiene que trabajar para lograr un funcionamiento personal más sano.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="niños" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Trabajo Gestáltico con Niños y Adolescentes</h4>
      </div>
      <div class="modal-body">
        <p>
          Utilizando las herramientas gestálticas y la terapia de juego se acompaña a los niños y a los adolescentes, propiciando que puedan <strong>integrar las experiencias difíciles de su vida</strong> para que logren <strong>desarrollar más autocontrol, autonomía,  aumento de autoestima y de salud emocional</strong>.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="transpersonal" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Psicoterapia Transpersonal</h4>
      </div>
      <div class="modal-body">
        <p>
          Se enfoca en el trabajo interno. <strong>Atiende la dimensión espiritual del ser</strong>, centrándose en el proceso del autoconocimiento y en específico, en los <strong>temas existenciales</strong>: ¿Quién soy?, ¿cuál es el sentido del sufrimientos y de las pérdidas?, y ¿cuál es el sentido de la vida?
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="gestalt" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">
          Sensibilización Gestalt</h4>
      </div>
      <div class="modal-body">
        <p>
          Es una técnica semi-estructurada que promueve “el darse cuenta y la autorresponsabilidad”. Su objetivo es que la persona esté en <strong>contacto consigo misma</strong> y con el mundo que le rodea y se trabaja utilizando  <strong>dinámicas vivenciales</strong>.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="sintoma" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Trabajo Gestáltico con el Síntoma</h4>
      </div>
      <div class="modal-body">
<p style="text-align: justify;">Adriana Schnake es la creadora de esta técnica y concluye en sus investigaciones que la enfermedad o los síntomas de la misma no son nuestros enemigos; sino que éstos aparecen para dar un mensaje. </p>
<p style="text-align: justify;">A través de la <b>ampliación de la conciencia</b>, el <b>contacto y </b> utilizando un <b>proceso de sensibilización</b> se concientizan estos mensajes, pudiéndose trabajar con el órgano dechoque u órgano afectado.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="tanatologia" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Tanatología</h4>
      </div>
      <div class="modal-body">
        <p>
          Es el conjunto de técnicas y actitudes aplicadas para el acompañamiento, durante el proceso de una <strong>pérdida o duelo</strong>.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="programacion" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Programación Neurolingüística</h4>
      </div>
      <div class="modal-body">
        <p>
          Consiste en una herramienta de transformación personal para alcanzar un crecimiento individual, retando y re-encuadrando el sistema de creencias y los mapas mentales. <strong>Se centra en la forma en la que nuestros cinco sentidos filtran nuestras experiencias del mundo exterior y de cómo usamos los sentidos interiores</strong>; o sea; nuestros pensamientos, la percepción, la imaginación, nuestros patrones de creencias, los cuales determinan lo que hacemos y lo que conseguimos, consciente e inconscientemente, <strong>para obtener los resultados que deseamos</strong>.
        </p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="constelaciones" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog psicoterapia">
    <div class="modal-content psicoterapia">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Constelaciones Familiares</h4>
      </div>
      <div class="modal-body">
        <p>
          Esta herramienta terapéutica, particularmente eficaz y a la que las demás escuelas terapéuticas oponen resistencia, fue creada por Bert Hellinger. Se basa en los fundamentos de la Gestalt, la Programación Neurolingüística (PNL), la hipnosis y la Dinámica de Grupos. Su creador asevera que, aunque la persona no conozca a sus padres biológicos o no se encuentre cerca de ellos se heredan algunos factores físicos, como son: el color de los ojos y de la piel; aptitudes, patrones de conducta y enfermedades; así como  vivencias emocionales, positivas o negativas, hasta de 72 generaciones anteriores, y que pueden ser difíciles de manejar por la persona.
        </p>
        <p>
          Lo que se logra con las constelaciones familiares es <strong>reordenar y reequilibrar</strong> el sistema de relaciones, permitiendo <strong>descubrir y sanar los vínculos familiares destructivos</strong>.

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<style media="screen">
  .modal-body{
    color:#fff;
  }
</style>

<?php require 'footer.php';?>
