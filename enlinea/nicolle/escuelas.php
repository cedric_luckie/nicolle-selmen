<?php require 'header.php';?>
<div class="topBanner">
  <img src="img/banners/escuelas.jpg" alt="Banner escuelas" width="100%"/>
</div>


  <div class="air"></div>


  <div class="container">
    <div class="contenido">



      <div class="row">
        <div class="col-md-7">
          <h2 class="texto_morado">
            <img class="icono" src="img/iconos/escuelas.png" alt="Icono escuelas" />
            Escuelas</h2>

          <p>
            La <font>educación</font> es un <font>reto constante</font> y requiere de suma dedicación.
Las condiciones de vida cambian constantemente: no es igual la visión educativa de hace 15 o 20 años, a la de  hoy; por lo que es de suma importancia que  las <font>instituciones educativas se actualicen</font> detectando sus necesidades, capacidades y demandas pedagógicas, de forma que puedan utilizar <font>técnicas de vanguardia</font> en el proceso de enseñanza-aprendizaje, propiciando que el <font>alumno desarrolle estrategias</font> para generar <font>aprendizajes significativos</font>  y relaciones personales o grupales que favorezcan el <font>crecimiento de su potencial</font>.
          </p>
          <p>
            Los talleres de capacitación marcan la diferencia, ya que impactan positivamente al centro educativo reflejando una ventaja sobre otras instituciones con las cuales se tiene que competir. Los cambios son visibles y continuos, dentro de la institución y de su entorno.
          </p>

        </div>


        <div class="col-md-5 lateral">
          <div class="panel panel-default">
            <div class="panel-heading escuelas">
              <h3 class="panel-title">SERVICIOS PARA ESCUELAS</h3>
            </div>
            <div class="list-group">
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#impacto">Talleres a la medida de Alto Impacto</a>
              <a class="list-group-item" href="javascript:;" data-toggle="modal" data-target="#desarrollo">Desarrollo organizacional</a>

            </div>
          </div>
          <img src="img/escuelas/1.jpg" alt="Escuelas" width="100%"/>
          <div class="space"></div>
          <img src="img/escuelas/3.jpg" alt="Escuelas" width="100%"/>
        </div>

      </div>

    </div>
  </div><!-- /.container -->

  <?php require 'testimonios.php';?>
  <?php require 'servicios.php';?>




<div class="modal fade" id="impacto" tabindex="-1" role="dialog" aria-labelledby="sintoma" aria-hidden="true">
  <div class="modal-dialog escuelas">
    <div class="modal-content escuelas">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Talleres a la medida de Alto Impacto</h4>
      </div>
      <div class="modal-body">
        <p>
          Estos talleres propician la <font>potenciación de las aptitudes de los docentes</font>. Les orienta sobre la forma de <font>trabajo específico y personal</font> con cada uno de sus <font>alumnos</font>; estimula su <font>actividad</font> y <font>creatividad</font> para que apoye a los educandos en su crecimiento personal y en el proceso de aprendizaje, convirtiéndose en un excelente <font>facilitador</font> que considera las necesidades y niveles de cada uno de ellos, logrando que desarrollen las competencias para la vida. Conseguir que los <font>padres</font> y el personal de la institución se sientan <font>comprometidos e involucrados</font> con los principios y los objetivos del centro educativo, provocando una transformación que perdure y, apoya para que el desarrollo de las <font>conferencias y juntas</font> con los padres y personal de la institución sean <b>exitosas</b> y de alto impacto, beneficiando a la comunidad escolar y extraescolar.
        </p>
        <p>
          Para lograrlo, es necesario identificar las necesidades específicas de su centro educativo, de manera que se elijan los talleres que satisfagan los requerimientos de su institución.
        </p>
        <b>TALLERES PARA DOCENTES:</b>
        <br/>- Desarrollo del Potencial Docente.<br/>- Inteligencia Emocional.<br/>- Sanando mis heridas para no engancharme con mis alumnos.<br/>- Yo en relación a mis alumnos.<br/>- Manejo del conflicto. <br/>- Estilos de Aprendizaje.<br/>- Estrategias de Enseñanza basadas en la Educación           centrada en la Persona.<br/>- Herramientas de PNL en la Educación. <br/>- Desarrollo de Habilidades para el Pensamiento Crítico y Creativo.<br/>- ¿Cómo generar un aprendizaje significativo?<br/>- El desarrollo de la inteligencia emocional en la Didáctica.<br/><br/>
        <b>TALLERES PARA PADRES:</b>
        <br/>- Disciplina asertiva.<br/>- Conectándome con mi hijo.<br/>- La Rueda de la Vida.<br/>- ¿Cómo me relaciono con la vida y que hay detrás?<br/>- Inteligencia Emocional.<br/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<style media="screen">
.modal-body{
  color:#fff;
  padding:20px;
}

.modal-dialog{
  width:700px;
}
</style>


<div class="modal fade" id="desarrollo" tabindex="-1" role="dialog" aria-labelledby="sintoma" aria-hidden="true">
  <div class="modal-dialog escuelas">
    <div class="modal-content escuelas">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title  " id="myModalLabel">Desarrollo organizacional</h4>
      </div>
      <div class="modal-body">
        <p>
          Este programa es el más recomendado cuando se quiere <b>potenciar a la institución educativa, abarcando todas las áreas y a todo el personal</b>. Para ello se utiliza  un <b>diagnóstico cuantitativo y cualitativo</b>, con el que se  identifican las <b>áreas de oportunidad</b>  que existen.
        </p>
        <p>
          Se cuenta con un Sistema de Semaforización que indica la situación real de la institución, usando el siguiente señalamiento:
        </p>
         <br/>• Verde (Estado Sano)
         <br/>• Amarillo (Estado de Alerta)
         <br/>• Rojo (Estado de Peligro)
        <br><br>
        <p>
          El Sistema de Semaforización arroja <b>resultados cuantitativos</b>, utilizando un cuestionario que evalúa <b>catorce factores</b> fundamentales de la institución educativa, a nivel <b>Estructural y Social</b>. En una entrevista individual, a los colaboradores de la institución se les aplica un <b>Cuestionario</b> de Detección de Necesidades de Capacitación que finalmente arrojará los <b>resultados cualitativos</b>.
        </p>
        <p>
          El Desarrollo Organizacional maneja prácticas innovadoras y de vanguardia para apoyar a los directivos, coordinadores, maestros, al personal administrativo y a todo aquel que forme parte de la institución para que desarrollen las habilidades que les permita desempeñarse con éxito en su función, dentro  el área que les corresponde.
        </p>
        <p>
          Este programa se divide en cinco fases, teniendo una duración mínima de tres meses:
        </p>
         Fase 1: Diagnostico y Detección de Necesidades de Capacitación.
         <br/>Fase 2: Entrega de Resultados y Plan de Trabajo.
         <br/>Fase 3: Implementación del Plan de Trabajo.
         <br/>Fase 4: Evaluación y Resultados.
         <br/>Fase 5: Seguimiento

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<?php require 'footer.php';?>
