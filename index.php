<?php require 'header.php';?>
  <div class="flexslider" style="margin-top:-20px;z-index:999;position:relative;width:100%">
    <ul class="slides" style="width:100%;height:300px;">
      <li>
        <a href="./social" title="Social Media">
          <img src="img/banners/social.jpg" width="100%" />
        </a>
        <div class="slide_text">
          <div class="slide_title">Social Media</div>
        </div>
      </li>
      <li>
        <a href="./deportistas" title="Deportistas">
          <img src="img/banners/deportistas2.jpg" width="100%" />
        </a>        
        <div class="slide_text">
          <div class="slide_title">Deportistas</div>
        </div>
      </li>
      <li>
        <a href="./psicoterapia" title="Psicoterapia">
          <img src="img/banners/psico.jpg" width="100%" />
        </a>   
          <div class="slide_text">
            <div class="slide_title">Psicoterapia & Coaching</div>
          </div>
        
      </li>
      <li>
        <a href="./talleres" title="Talleres">
          <img src="img/banners/HOME_talleres_2.jpg" width="100%" />  
        </a>
          <div class="slide_text">
            <div class="slide_title">Talleres al público</div>
          </div>        
      </li>
      <li>
        <a href="./escuelas" title="Escuelas">
          <img src="img/banners/HOME_ESCUELAS_1.jpg" width="100%" />
        </a>
          <div class="slide_text">
            <div class="slide_title">Escuelas</div>
          </div>        
      </li>
      <li>
        <a href="./empresas" title="Empresas">
          <img src="img/banners/HOME_EMPRESAS_1.jpg" width="100%" />
        </a>        
        <div class="slide_text">
          <div class="slide_title">Empresas</div>
        </div>
      </li>
      <li>
        <a href="./conferencias" title="Conferencias">
          <img src="img/banners/conferencias.jpg" width="100%" />
        </a>        
        <div class="slide_text">
          <div class="slide_title">Conferencias vivenciales</div>
        </div>
      </li>


    </ul>
  </div>
  <div class="air"></div>


        <div class="container">
          <div class="contenido">

            <div class="row">
              <div class="col-md-12">
                <p align="justify">
                  Soy Nicolle Selmen Chattaj, Lic. en Desarrollo Humano, Psicoterapeuta Humanista y Coach Certificado. Tengo más de 7 años de experiencia acompañando al ser humano en su crecimiento. 
                </p>

                <p align="justify">
                  Promuevo el descubrimiento y desarrollo del potencial humano, a nivel individual y organizacional a través de talleres, grupos de sensibilización, conferencias y programas de desarrollo organizacional; utilizando técnicas de la Psicoterapia Especializada y del Coaching. 
                </p>

                <p align="justify">
                  Descubrir y desarrollar el potencial del ser humano consiste, básicamente, en ampliar los tres aspectos fundamentales del mismo, siendo éstos: el contacto, la autoconciencia y la auto-responsabilidad.
                </p>
                

                <div class="btn-group">

                  <button class="btn btn-default texto_blanco morado" data-toggle="modal" data-target="#contactoModal">Contacto</button>
                  <button class="btn btn-default texto_blanco naranja" data-toggle="modal" data-target="#concienciaModal">Conciencia</button>
                  <button class="btn btn-default texto_blanco amarillo" data-toggle="modal" data-target="#respModal">Auto-responsabilidad</button>
                </div>
              </div>

              <!--<div class="col-md-6">
                <iframe src="http://www.youtube.com/embed/?listType=user_uploads&list=UCBLHG3yzxea5bKf3GiW6irw" width="100%" height="300"></iframe>
              </div>-->
            </div>

          </div>
        </div><!-- /.container -->

        <?php require 'testimonios.php';?>

<style media="screen">
.col-md-4 img{
  max-width:100%;
  text-align:left;
}
</style>
        <div class="container">
          <div class="contenido">
            <h2>Servicios</h2>
            <hr>
            <div class="row">
              <div class="col-sm-4 feature">
                  <a href="psicoterapia">
                    <img src="./img/psicoterapia1.png" width="270px" alt="Psicoterapia & Coaching"/>
                  </a>
              </div>
              <div class="col-sm-4 feature">
                <a href="talleres">
                  <img src="./img/talleres1.png" width="205px" alt="Talleres al público"/>
                </a>
              </div>
              <div class="col-sm-4 feature">
                <a href="escuelas">
                  <img src="./img/escuelas1.png" width="190px" alt="Escuelas"/>
                </a>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-4 feature">
                <a href="empresas">
                  <img src="./img/empresas1.png" width="200px" alt="Empresas"/>
                </a>
              </div>
              <div class="col-md-4 feature">
                <a href="deportistas">
                  <img src="./img/deportistas1.png" width="220px" alt="Deportistas"/>
                </a>
              </div>
              <div class="col-md-4 feature">
                <a href="conferencias">
                  <img src="./img/conferencias1.png" width="255px" alt="Conferencias Vivenciales"/>
                </a>
              </div>
            </div>

          </div>
        </div>























        <!-- Modals -->
        <div class="modal fade" id="contactoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog morado">
            <div class="modal-content morado">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title texto_blanco" id="myModalLabel">El contacto</h4>
              </div>
              <div class="modal-body texto_blanco">
                Es la capacidad del ser humano de tener un encuentro con “lo otro”, y se logra estando consciente, exterior e interiormente, en el momento  de hacer el contacto, a través de los cinco sentidos, de las sensaciones y de los sentimientos.
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="concienciaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog morado">
            <div class="modal-content naranja">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title texto_blanco" id="myModalLabel">La conciencia</h4>
              </div>
              <div class="modal-body texto_blanco">
                Consiste en “darse cuenta” de las acciones y reacciones personales y se alcanza observando, explorando y reflexionando, en todo momento.
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="respModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog amarillo">
            <div class="modal-content amarillo">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title texto_blanco" id="myModalLabel">La auto-responsabilidad</h4>
              </div>
              <div class="modal-body texto_blanco">
                Es la “capacidad de respuesta del ser humano” ante lo que hace o deja de hacer, dice o no dice, siente o no siente y que origina una “respuesta” de la cual habrá de responsabilizarse, haciéndose cargo de sí mismo.
Cuando la persona es “auto-responsable” se da la oportunidad de conocer su poder interno para determinar sus actos, su respuesta y las consecuencias de las mismas, evitando que otros tengan que responsabilizarse de lo que le corresponde y, por supuesto, también le facilita el no apropiarse de las responsabilidades de otros.
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>



<?php require 'footer.php';?>
